
class Hooks {
  constructor() {
    this.hooks = [];
  }

  add(hook) {
    if (typeof hook !== 'function') {
      throw new TypeError('Argument "hook" must be a function')
    }
    this.hooks.push(hook);
  }

  delete(hook) {
    const ind = this.hooks.findIndex(val => val === hook);
    if (ind < 0) {
      return false;
    }

    this.hooks = this.hooks.splice(ind, 1);
    return true;
  }

  process(val) {
    this.hooks.forEach(hook => hook && hook(val));
  }
}

module.exports = Hooks;