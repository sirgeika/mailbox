const Hooks = require('./hooks');
const PreHooks = require('./pre-hooks');

class HooksHandler {
  constructor() {
    this._pres = {};
    this._posts = {};
  }

  pre(name, fn) {
    this._pres[name] = this._pres[name] || new PreHooks();
    this._pres[name].add(fn);
    this[name] = this.hookFun(name);
  }

  post(name, fn) {
    this._posts[name] = this._posts[name] || new Hooks();
    this._posts[name].add(fn);

    this[name] = this.hookFun(name);
  }

  hookFun(name) {
    return function() {
      let res = arguments[0];
      const args = Array.prototype.slice.call(arguments);
      const pres = this._pres[name];
      if (pres) {
        res = pres.process.apply(pres, args);
      }

      if (res) {
        const resFun = this.__proto__[name].call(this, res);
        if (resFun) {
          this._posts[name] && this._posts[name].process(res);
        }
        return res;
      }
    };
  }
}

module.exports = HooksHandler;