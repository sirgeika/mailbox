const HooksHandler = require('./hooks-handler');

const sendToConsole = (mail) => {
  if (mail) {
    console.log(mail);
    return true;
  }
};

const consoleSender = {
  send: sendToConsole
};

class Mailbox extends HooksHandler {
  constructor(name, sender) {
    super();
    this.name = name;
    this.sender = sender || consoleSender;
  }

  pre(fn) {
    super.pre('sendMail', fn);
  }

  notify(fn) {
    super.post('sendMail', fn);
  }

  sendMail(mail) {
    return this.sender.send(mail);
  }
}

module.exports = Mailbox;