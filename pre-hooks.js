const Hooks = require('./hooks');

class PreHooks extends Hooks {
  next(val, callback) {
    const fn = (ind, val) => {
      if (this.hooks.length <= ind) {
        return callback && callback(val);
      }
      const hook = this.hooks[ind];
      if (hook) {
        return hook(val, fn.bind(this, ind + 1));
      }
      return fn(ind + 1, val);
    };

    return fn(0, val);
  }

  process(val) {
    let mail;
    this.next(val, modify => mail = modify);
    return mail;
  }
}

module.exports = PreHooks;