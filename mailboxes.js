const Mailbox = require('./mailbox');

const mailboxes = {};

function mailbox(name) {
  if (mailboxes[name]) {
    return mailboxes[name];
  }

  const box = new Mailbox(name);
  return mailboxes[name] = box;
}

module.exports = mailbox;
